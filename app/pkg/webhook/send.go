package webhook

import (
	svix "github.com/svix/svix-webhooks/go"
)

type Message struct {
	EventID   string `json:"event_id"`
	EventType string `json:"event_type"`
	Payload   struct {
		ID     int    `json:"id"`
		Status string `json:"status"`
	}
}

func (message *Message) Send() error {
	svixClient := svix.New("AUTH_TOKEN", nil)
	app, err := svixClient.Application.Create(&svix.ApplicationIn{
		Name: "Application name",
	})
	if err != nil {
		return err
	}
	svixClient.Message.Create(app.Id, &svix.MessageIn{
		EventId:   svix.String(message.EventID),
		EventType: message.EventType,
		Payload: map[string]interface{}{
			"id":     message.Payload.ID,
			"status": message.Payload.Status,
		},
	})
	return nil
}
