package utils

import (
	"fmt"
	"math"
)

func TimeAsString(dt float64) string {
	dtTime := dt
	hours := math.Floor(dtTime / 3600)
	minutes := math.Ceil(math.Mod(dtTime, 3600)/60) - 1
	seconds := int(dtTime) % 60

	return fmt.Sprintf("%02v:%02v:%02v", hours, minutes, seconds)
}
