package utils

import (
	"fmt"
	"os"
	"strings"
)

func BaseDir() string {
	basePath, err := os.Getwd()
	if err != nil {
		panic("current dir not found")
	}

	fmt.Println(basePath)
	if strings.HasSuffix(basePath, "/build") {
		basePath = basePath + "/.."
	}

	if strings.HasSuffix(basePath, "/app") {
		basePath = basePath + "/.."
	}

	return basePath
}

func BaseDirFile(file string) string {
	return BaseDir() + "/" + file
}
