package mysql

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

type StorageConfig struct {
	Host     string
	Port     string
	Database string
	Username string
	Password string
}

type StorageClient struct {
	DB *sql.DB
}

func NewClient(ctx context.Context, cfg *StorageConfig) (*StorageClient, error) {
	conn := fmt.Sprintf("%s:%s@/%s?parseTime=true", cfg.Username, cfg.Password, cfg.Database)
	db, err := sql.Open("mysql", conn)
	if err != nil {
		return nil, err
	}
	// See "Important settings" section.
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return &StorageClient{
		DB: db,
	}, nil
}
