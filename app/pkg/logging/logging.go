package logging

import (
	"fmt"
	"github.com/dposkachei/go-aws/app/internal/config"
	"github.com/dposkachei/go-aws/app/pkg/utils"
	"github.com/sirupsen/logrus"
	"io"
	"os"
	"path"
	"runtime"
	"time"
)

const (
	layoutISO = "2006-01-02"
)

type writeHook struct {
	Writer    []io.Writer
	LogLevels []logrus.Level
}

func (hook *writeHook) Fire(entry *logrus.Entry) error {
	var err error
	line, err := entry.String()
	if err != nil {
		return err
	}
	for _, w := range hook.Writer {
		_, errW := w.Write([]byte(line))
		if errW != nil {
			return errW
		}
	}
	return err
}

func (hook *writeHook) Levels() []logrus.Level {
	return hook.LogLevels
}

var e *logrus.Entry

type Logger struct {
	*logrus.Entry
}

func GetLogger() *Logger {
	return &Logger{e}
}

func (l *Logger) GetLoggerWithField(k string, v interface{}) *Logger {
	return &Logger{l.WithField(k, v)}
}

func init() {
	cfg := config.GetConfig()
	dt := time.Now()
	prefix := dt.Format(layoutISO)

	l := logrus.New()
	l.SetReportCaller(true)
	l.Formatter = &logrus.TextFormatter{
		CallerPrettyfier: func(frame *runtime.Frame) (function string, file string) {
			filename := path.Base(frame.File)
			return fmt.Sprintf("%s()", frame.Function), fmt.Sprintf("%s:%d", filename, frame.Line)
		},
		DisableColors: false,
		FullTimestamp: true,
	}

	allFile, err := os.OpenFile(utils.BaseDirFile("logs/"+prefix+".log"), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0640)
	if err != nil {
		panic(err)
	}

	l.SetOutput(io.Discard)

	if cfg.Logger.Enabled {
		l.AddHook(&writeHook{
			//Writer: []io.Writer{allFile},
			Writer:    []io.Writer{allFile, os.Stdout}, // для вывода в консоли
			LogLevels: logrus.AllLevels,
		})
	} else {
		l.AddHook(&writeHook{
			Writer:    []io.Writer{allFile},
			LogLevels: logrus.AllLevels,
		})
	}
	l.SetLevel(logrus.TraceLevel)
	e = logrus.NewEntry(l)
}

func (l *Logger) Info(args ...interface{}) {
	fmt.Println(fmt.Sprintf("\033[1;34m%s\033[0m", "info: ") + fmt.Sprintf("%s", args...))
	l.Entry.Info(args...)
}

func (l *Logger) InfoSi(args ...interface{}) {
	l.Entry.Info(args...)
}

func (l *Logger) Success(args ...interface{}) {
	fmt.Println(fmt.Sprintf("\033[1;32m%s\033[0m", "success: ") + fmt.Sprintf("%s", args...))
	l.Entry.Info(args...)
}

func (l *Logger) Error(args ...interface{}) {
	fmt.Println(fmt.Sprintf("\033[1;31m%s\033[0m", "error: ") + fmt.Sprintf("%s", args...))
	l.Entry.Error(args...)
}

func (l *Logger) SuccessSi(args ...interface{}) {
	l.Entry.Info(args...)
}

func (l *Logger) Default(args ...interface{}) {
	fmt.Println(fmt.Sprintf("%s", args...))
	l.Entry.Info(args...)
}
