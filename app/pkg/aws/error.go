package aws

var (
	ErrExists = NewAwsError(nil, "file exists", "", "US-000003")
)

type AppAwsError struct {
	Err              error  `json:"-"`
	Message          string `json:"message,omitempty"`
	DeveloperMessage string `json:"developerMessage,omitempty"`
	Code             string `json:"code,omitempty"`
}

//
//func awsErrorExists(err error) *AppAwsError {
//	return NewAwsError(err, "file exists", err.Error(), "US-000000")
//}

func (e *AppAwsError) Is(err error) bool {
	target, ok := err.(*AppAwsError)
	if !ok {
		return false
	}
	return e.Code == target.Code
}

func NewAwsError(err error, message, developerMessage, code string) *AppAwsError {
	return &AppAwsError{
		Err:              err,
		Message:          message,
		DeveloperMessage: developerMessage,
		Code:             code,
	}
}

func (e *AppAwsError) Error() string {
	return e.Message
}

func (e *AppAwsError) Unwrap() error {
	return e.Err
}
