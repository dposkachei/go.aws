package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"os"
	"strings"
)

type ClientConfig struct {
	KeyID     string
	SecretKey string
	Bucket    string
	Region    string
	Host      string
	PathFrom  string
	PathTo    string
	Enabled   bool
}

func ConnectAws(cfg ClientConfig) *session.Session {
	sess, err := session.NewSession(
		&aws.Config{
			Region:   aws.String(cfg.Region),
			Endpoint: aws.String(cfg.Host),
			Credentials: credentials.NewStaticCredentials(
				cfg.KeyID,
				cfg.SecretKey,
				"", // a token will be created when the session it's used.
			),
		})
	if err != nil {
		panic(err)
	}
	return sess
}

func UploadFiles(sess *session.Session, cfg ClientConfig, file string) error {
	fileData, err := os.Open(file)
	if err != nil {
		return err
	}
	defer fileData.Close()

	filePath := strings.Replace(file, cfg.PathTo, "", 1)

	exists := checkFileExists(sess, cfg, filePath)
	if exists != nil {
		return exists
	}
	_, err = s3.New(sess).PutObject(&s3.PutObjectInput{
		Bucket: aws.String(cfg.Bucket),
		Key:    aws.String(filePath),
		Body:   fileData,
	})
	if err != nil {
		return err
	}
	return nil
}

func checkFileExists(sess *session.Session, cfg ClientConfig, file string) *AppAwsError {
	//var appErr *AppAwsError
	res, err := s3.New(sess).GetObject(&s3.GetObjectInput{
		Bucket: aws.String(cfg.Bucket),
		Key:    aws.String(file),
	})
	// error read
	if err != nil && res.Body != nil {
		return NewAwsError(err, err.Error(), "", "")
	}
	if res.Body != nil {
		defer res.Body.Close()
		return NewAwsError(err, "file exists", "", "")
	}
	return nil
}
