package aws

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/schollz/progressbar/v3"
	"sync"
)

func UploadFile(wg *sync.WaitGroup, sess *session.Session, cfg ClientConfig, bar *progressbar.ProgressBar, file string) error {
	defer wg.Done()
	var err error
	err = UploadFiles(sess, cfg, file)
	if err != nil {
		errBar := bar.Add(1)
		if errBar != nil {
			return errBar
		}
		return err
	}
	err = bar.Add(1)
	if err != nil {
		return err
	}
	return nil
}
