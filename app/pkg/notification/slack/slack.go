package slack

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

const DefaultSlackTimeout = 5 * time.Second

type Client struct {
	WebHookURL string
	UserName   string
	Channel    string
	TimeOut    time.Duration
}

type Message struct {
	Username string `json:"username"`
	Text     string `json:"text"`
}

func (sc Client) SendInfo(message string) (err error) {
	return sc.sendHTTPRequest(Message{
		Text: message,
	})
}

func (sc Client) sendHTTPRequest(slackRequest Message) error {
	var err error
	slackBody, _ := json.Marshal(slackRequest)
	//slackBody, _ := json.Marshal(slackRequest)
	ctx := context.TODO()
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, sc.WebHookURL, bytes.NewBuffer(slackBody))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	if sc.TimeOut == 0 {
		sc.TimeOut = DefaultSlackTimeout
	}
	client := &http.Client{Timeout: sc.TimeOut}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return err
	}
	fmt.Println(buf)
	if buf.String() != "ok" {
		return errors.New("non-ok response returned from slack")
	}
	return nil
}
