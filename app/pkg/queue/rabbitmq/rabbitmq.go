package rabbitmq

import (
	"bytes"
	"fmt"
	"github.com/dposkachei/go-aws/app/pkg/logging"
	"github.com/google/uuid"
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
	"time"
)

const (
	TYPE_AWS_SYNC = "aws-sync"
)

type ClientConfig struct {
	Host     string
	Port     string
	Username string
	Password string
	Database string
	Queue    string
}

type RabbitClient struct {
	sendConn *amqp.Connection
	recConn  *amqp.Connection
	sendChan *amqp.Channel
	recChan  *amqp.Channel
	Connect  string
	Queue    string
}

type Message struct {
	Type    string `json:"type"`
	Message string `json:"message"`
	ID      string `json:"id"`
}

func NewClient(cfg *ClientConfig) *RabbitClient {
	return &RabbitClient{
		Connect: fmt.Sprintf("amqp://%s:%s@%s:%s/", cfg.Username, cfg.Password, cfg.Host, cfg.Port),
		Queue:   cfg.Queue,
	}
}

// Создаём подключение к rabbitmq
func (rcl *RabbitClient) connect(isRec, reconnect bool) (*amqp.Connection, error) {
	var err error
	logger := logging.GetLogger()
	if reconnect {
		if isRec {
			rcl.recConn = nil
		} else {
			rcl.sendConn = nil
		}
	}
	if isRec && rcl.recConn != nil {
		return rcl.recConn, nil
	} else if !isRec && rcl.sendConn != nil {
		return rcl.sendConn, nil
	}
	c := rcl.Connect
	conn, err := amqp.Dial(c)
	if err != nil {
		logger.Fatal("\r\n--- could not create a conection ---\r\n")
		time.Sleep(1 * time.Second)
		return nil, err
	}
	//defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		logger.Fatal("failed to connect to channel")
		return nil, err
	}
	//defer ch.Close()
	if isRec {
		rcl.recConn = conn
		rcl.recChan = ch
		return rcl.recConn, nil
	} else {
		rcl.sendConn = conn
		rcl.sendChan = ch
		return rcl.sendConn, nil
	}
}

// Подписываемся исходя из имени очереди
func (rcl *RabbitClient) Consume(f func(interface{}) error) {
	for {
		for {
			_, err := rcl.connect(true, true)
			if err == nil {
				break
			}
		}
		log.Printf("--- connected to consume '%s' ---\r\n", rcl.Queue)
		q, err := rcl.recChan.QueueDeclare(
			rcl.Queue,
			true,
			false,
			false,
			false,
			nil,
			//amqp.Table{"x-queue-mode": "lazy"},
		)
		if err != nil {
			log.Println("--- failed to declare a queue, trying to reconnect ---")
			time.Sleep(1 * time.Second)
			continue
		}
		connClose := rcl.recConn.NotifyClose(make(chan *amqp.Error))
		connBlocked := rcl.recConn.NotifyBlocked(make(chan amqp.Blocking))
		chClose := rcl.recChan.NotifyClose(make(chan *amqp.Error))
		m, err := rcl.recChan.Consume(
			q.Name,
			uuid.NewString(),
			false,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			log.Println("--- failed to consume from queue, trying again ---")
			time.Sleep(1 * time.Second)
			continue
		}
		shouldBreak := false
		for {
			if shouldBreak {
				break
			}
			select {
			case _ = <-connBlocked:
				log.Println("--- connection blocked ---")
				shouldBreak = true
				break
			case err = <-connClose:
				log.Println("--- connection closed ---")
				shouldBreak = true
				break
			case err = <-chClose:
				log.Println("--- channel closed ---")
				shouldBreak = true
				break
			case d := <-m:
				err := f(d.Body)
				if err != nil {
					_ = d.Ack(false)
					break
				}
				_ = d.Ack(true)
			}
		}
	}
}

// Публикуем байтовый массив в очередь
func (rcl *RabbitClient) Publish(b []byte) {
	r := false
	//defer rcl.sendChan.Close()
	for {
		for {
			_, err := rcl.connect(false, r)
			if err == nil {
				break
			}
		}
		fmt.Println(rcl.Queue)
		q, err := rcl.sendChan.QueueDeclare(
			rcl.Queue,
			true,
			false,
			false,
			false,
			nil,
			//amqp.Table{"x-queue-mode": "lazy"},
		)
		if err != nil {
			log.Println("--- failed to declare a queue, trying to resend ---")
			r = true
			continue
		}
		err = rcl.sendChan.Publish(
			"",
			q.Name,
			false,
			false,
			amqp.Publishing{
				MessageId:    uuid.NewString(),
				DeliveryMode: amqp.Persistent,
				ContentType:  "text/plain",
				Body:         b,
			})
		if err != nil {
			log.Println("--- failed to publish to queue, trying to resend ---")
			r = true
			continue
		}
		break
	}
}

func Listen(cfg *ClientConfig) {
	logger := logging.GetLogger()
	// rabbitmq
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/", cfg.Username, cfg.Password, cfg.Host, cfg.Port))
	if err != nil {
		logger.Fatalf("failed to dial to amqp://%s:%s@%s:%s/", cfg.Username, cfg.Password, cfg.Host, cfg.Port)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		logger.Fatal("failed to connect to channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(cfg.Queue, true, false, false, false, nil)
	if err != nil {
		logger.Fatalf(`failed to declare queue "%s" to channel`, cfg.Queue)
	}
	err = ch.Qos(1, 0, false)
	if err != nil {
		logger.Fatal("failed to qos channel")
	}
	msgs, err := ch.Consume(q.Name, "", false, false, false, false, nil)
	if err != nil {
		logger.Fatalf(`failed to consume by name "%s"`, q.Name)
	}
	forever := make(chan bool)
	go func() {
		for d := range msgs {
			logger.Infof("Received a message: %s", d.Body)
			dotCount := bytes.Count(d.Body, []byte("."))
			t := time.Duration(dotCount)
			time.Sleep(t * time.Second)
			logger.Infof("Done")
			d.Ack(false)
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever
}
