package redis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
)

type handler struct {
	client *redis.Client
}

type ClientConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database int
}

func NewClient(cfg *ClientConfig) *handler {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", cfg.Host, cfg.Port),
		Password: cfg.Password,
		DB:       cfg.Database,
	})
	return &handler{
		client: client,
	}
}

func (h handler) Set(ctx context.Context, key string, value string) error {
	err := h.client.Set(ctx, key, value, 0).Err()
	if err != nil {
		return err
	}
	return nil
}

func (h handler) Get(ctx context.Context, key string) (string, error) {
	val, err := h.client.Get(ctx, key).Result()
	if err != nil {
		return "", err
	}
	return val, nil
}

func Set(ctx context.Context, client *redis.Client, key string, value string) error {
	err := client.Set(ctx, key, value, 0).Err()
	if err != nil {
		return err
	}
	return nil
}

func Get(ctx context.Context, client *redis.Client, key string) (string, error) {
	val, err := client.Get(ctx, key).Result()
	if err != nil {
		return "", err
	}
	return val, nil
}
