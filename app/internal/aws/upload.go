package aws

import (
	"fmt"
	"github.com/dposkachei/go-aws/app/internal/config"
	"github.com/dposkachei/go-aws/app/internal/domain/sync_items"
	pkgAws "github.com/dposkachei/go-aws/app/pkg/aws"
	"github.com/dposkachei/go-aws/app/pkg/client/mysql"
	"github.com/dposkachei/go-aws/app/pkg/logging"
	"github.com/dposkachei/go-aws/app/pkg/notification/slack"
	"github.com/dposkachei/go-aws/app/pkg/utils"
	"github.com/schollz/progressbar/v3"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

const (
	layoutISO = "2006-01-02 15:04:05"
)

type ResultCounter struct {
	All     int
	Success int
	Error   int
	Exists  int
}

func UploadAll(clientMySQL *mysql.StorageClient) (ResultCounter, error) {
	var err error
	logger := logging.GetLogger()
	dtStart := time.Now()

	logger.Info("----------------------------------------------------------------------")
	logger.Info("current time: " + dtStart.Format(layoutISO))
	//fmt.Println("current time: " + dtStart.Format(layoutISO))

	//logger.Info("config initializing")
	cfg := config.GetConfig()
	//ctx := context.Background()

	//logger.Info("slack client initializing")
	client := slack.Client{
		WebHookURL: cfg.Slack.Webhook,
		UserName:   cfg.Slack.Username,
		Channel:    cfg.Slack.Channel,
		TimeOut:    0,
	}

	//logger.Info("slack send starting")
	err = sendSlack(client, logger, time.Now().Format(layoutISO)+" aws sync starting...")
	if err != nil {
		return ResultCounter{}, err
	}

	var files []string
	root := cfg.Aws.PathFrom

	//logger.Info("files initializing")
	err = filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			files = append(files, path)
		}
		return nil
	})
	if err != nil {
		return ResultCounter{}, err
	}

	//logger.Info("aws config initializing")
	awsClientCfg := pkgAws.ClientConfig{
		KeyID:     cfg.Aws.KeyID,
		SecretKey: cfg.Aws.SecretKey,
		Bucket:    cfg.Aws.Bucket,
		Region:    cfg.Aws.Region,
		Host:      cfg.Aws.Host,
		PathFrom:  cfg.Aws.PathFrom,
		PathTo:    cfg.Aws.PathTo,
		Enabled:   cfg.Aws.Enabled,
	}

	counters := ResultCounter{
		All: len(files),
	}
	var slackMessages []string
	//logger.Info("aws session initializing")

	bar := progressbar.Default(int64(len(files)))
	sess := pkgAws.ConnectAws(awsClientCfg)

	sem := make(chan struct{}, 4)

	var wg sync.WaitGroup
	for _, file := range files {
		wg.Add(1)
		go func(file string) {
			sem <- struct{}{}
			defer func() { <-sem }()

			var errSql error
			var existSynced bool

			//
			filePath := strings.Replace(file, cfg.Aws.PathTo+"/", "", 1)
			existSynced, errSql = sync_items.ExistSynced(clientMySQL, sync_items.SyncItem{
				Bucket: cfg.Aws.Bucket,
				Type:   "upload",
				Input:  file,
				Output: filePath,
			})
			if errSql != nil {
				logger.Error(errSql)
				return
			}

			if existSynced {
				defer wg.Done()
				errBar := bar.Add(1)
				if errBar != nil {
					logger.Error(errBar)
				}
				counters.Exists++
				return
			}
			//
			err = pkgAws.UploadFile(&wg, sess, awsClientCfg, bar, file)

			//
			_, errSql = sync_items.CreateSynced(clientMySQL, sync_items.SyncItem{
				Bucket: cfg.Aws.Bucket,
				Type:   "upload",
				Input:  file,
				Output: filePath,
			})
			if errSql != nil {
				logger.Error(errSql)
				return
			}

			if err != nil {
				if err.Error() == "file exists" {
					logger.InfoSi(fmt.Sprintf("sending file exists: %s", file))
					counters.Exists++
				} else {
					logger.InfoSi(fmt.Sprintf("sending file error: %s", file))
					logger.InfoSi(err.Error())
					counters.Error++
				}
			} else {
				logger.InfoSi(fmt.Sprintf("sending file success: %s", file))
				counters.Success++
			}
		}(file)
	}

	logger.Info("Waiting for workers to finish")
	wg.Wait()
	close(sem)
	logger.Success("Completed")

	dtFinish := time.Now()

	slackMessages = append(slackMessages, time.Now().Format(layoutISO))
	// logger.Info("current time: " + dtFinish.Format(layoutISO))
	// fmt.Println("current time: " + dtFinish.Format(layoutISO))

	out := dtFinish.Sub(dtStart).Seconds()
	logger.Info("time result: " + utils.TimeAsString(out))
	//fmt.Println("time result: " + utils.TimeAsString(out))

	slackMessages = append(slackMessages, fmt.Sprintf("success %d/%d", counters.Success, counters.All))
	logger.Info(fmt.Sprintf("success %d/%d", counters.Success, counters.All))
	//fmt.Println(fmt.Sprintf("success %d/%d", counters.Success, counters.All))

	slackMessages = append(slackMessages, fmt.Sprintf("error %d/%d", counters.Error, counters.All))
	logger.Info(fmt.Sprintf("error %d/%d", counters.Error, counters.All))
	//fmt.Println(fmt.Sprintf("error %d/%d", counters.Error, counters.All))

	slackMessages = append(slackMessages, fmt.Sprintf("exists %d/%d", counters.Exists, counters.All))
	logger.Info(fmt.Sprintf("exists %d/%d", counters.Exists, counters.All))
	//fmt.Println(fmt.Sprintf("exists %d/%d", counters.Exists, counters.All))

	err = sendSlack(client, logger, strings.Join(slackMessages, "\n"))
	if err != nil {
		return ResultCounter{}, err
	}
	logger.Info("----------------------------------------------------------------------")

	return counters, nil
}

func sendSlack(client slack.Client, logger *logging.Logger, message string) error {
	cfg := config.GetConfig()

	if !cfg.Slack.Enabled {
		return nil
	}
	//logger.Info("slack sending")
	err := client.SendInfo(message)
	if err != nil {
		logger.Fatal(err)
		return err
	}
	return nil
}
