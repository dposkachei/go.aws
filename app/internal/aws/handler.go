package aws

import (
	"encoding/json"
	"fmt"
	"github.com/dposkachei/go-aws/app/internal/apperror"
	"github.com/dposkachei/go-aws/app/internal/domain/sync_workers"
	actionCreate "github.com/dposkachei/go-aws/app/internal/domain/sync_workers/action/create"
	actionFind "github.com/dposkachei/go-aws/app/internal/domain/sync_workers/action/find"
	"github.com/dposkachei/go-aws/app/internal/handlers"
	"github.com/dposkachei/go-aws/app/pkg/client/mysql"
	"github.com/dposkachei/go-aws/app/pkg/logging"
	"github.com/dposkachei/go-aws/app/pkg/queue/rabbitmq"
	"github.com/julienschmidt/httprouter"
	"log"
	"net/http"
	"strconv"
	"time"
)

var _ handlers.Handler = &handler{}

const (
	// POST create sync_worker and start rabbitmq worker
	awsURL = "/aws"

	// POST create rabbitmq worker
	// awsSyncURL   = "/aws/sync"

	// GET show rabbitmq worker status
	awsIDURL = "/aws/:id"
)

type handler struct {
	logger         *logging.Logger
	clientRabbitMq *rabbitmq.RabbitClient
	storage        *mysql.StorageClient
}

type response struct {
	Success bool                   `json:"success"`
	Data    map[string]interface{} `json:"data"`
}

type responseItem struct {
	Success bool               `json:"success"`
	Data    syncWorkerResponse `json:"data"`
}

type syncWorkerResponse struct {
	ID         int    `json:"id"`
	Status     int    `json:"status"`
	StartedAt  string `json:"started_at"`
	FinishedAt string `json:"finished_at"`
}

func NewHandler(logger *logging.Logger, clientRabbitMq *rabbitmq.RabbitClient, storage *mysql.StorageClient) handlers.Handler {
	return &handler{
		logger:         logger,
		clientRabbitMq: clientRabbitMq,
		storage:        storage,
	}
}

func (h *handler) Register(router *httprouter.Router) {
	router.HandlerFunc(http.MethodGet, awsURL, apperror.Middleware(h.Index))
	router.HandlerFunc(http.MethodPost, awsURL, apperror.Middleware(h.Store))
	router.HandlerFunc(http.MethodGet, awsIDURL, apperror.Middleware(h.SyncId))
}

func (h *handler) Store(w http.ResponseWriter, r *http.Request) error {
	var err error
	//r.ParseForm()
	//fmt.Println(r.Form.Get("id"))
	w.Header().Set("Content-Type", "application/json")
	id, err := actionCreate.Handle(h.storage)
	if err != nil {
		log.Fatalf("Error create sync worker. Err: %s", err)
	}
	fmt.Println(fmt.Sprintf("Created sync_workers with id: %d", id))
	h.clientRabbitMq.Publish([]byte(fmt.Sprintf(`{"type": "%s", "id": "%d"}`, rabbitmq.TYPE_AWS_SYNC, id)))
	w.WriteHeader(200)

	// response
	resp := response{
		Success: true,
		Data: map[string]interface{}{
			"id": id,
		},
	}
	body, err := json.Marshal(resp)
	if err != nil {
		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	}
	fmt.Fprintf(w, string(body))
	return nil
}

func (h *handler) Index(w http.ResponseWriter, r *http.Request) error {
	w.Header().Set("Content-Type", "application/json")

	//fmt.Println(fmt.Sprintf("ping: %s", h.storage.DB.Ping()))

	_, err := sync_workers.GetAll(h.storage)
	if err != nil {
		panic(err)
	}

	//var res1 []string
	//for _, s := range results {
	//	//if s.Status == 1 {
	//	//
	//	//}
	//	//json, errM := json.Marshal(s.Message)
	//	//if errM != nil {
	//	//	log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	//	//}
	//	//res1 = append(res1, json)
	//}

	//var d map[interface{}]interface{}
	//d = append(d, items)
	resp := response{
		Success: true,
		Data:    make(map[string]interface{}, 0),
	}
	//var body []byte
	body, err := json.Marshal(resp)
	if err != nil {
		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	}
	w.WriteHeader(200)
	fmt.Fprintf(w, string(body))
	//
	//if err := json.Unmarshal(body, resp); err != nil {
	//	return err
	//}

	//w.Write(body)
	//fmt.Fprintf(w, string(body))

	//counters, err := UploadAll()
	//if err != nil {
	//	return err
	//}
	// w.Write([]byte(fmt.Sprintf("all: %d", counters.All)))
	// w.Write([]byte(fmt.Sprintf("success: %d", counters.Success)))
	// w.Write([]byte(fmt.Sprintf("error: %d", counters.Error)))
	// w.Write([]byte(fmt.Sprintf("exists: %d", counters.Exists)))
	//return apperror.ErrNotFound
	return nil

	//
	//w.Header().Set("Content-Type", "application/json")
	//
	//// aws sync start
	//h.clientRabbitMq.Publish([]byte("aws sync start"))

	//body, err := json.Marshal(resp)
	//if err != nil {
	//	log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	//}
	//w.WriteHeader(200)
	//fmt.Fprintf(w, string(body))
	//return nil
}

func (h *handler) SyncId(w http.ResponseWriter, r *http.Request) error {
	var err error
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)

	params := httprouter.ParamsFromContext(r.Context())

	id := params.ByName("id")
	var idInt int
	idInt, err = strconv.Atoi(id)
	if err != nil {
		w.WriteHeader(422)
		return nil
	}
	item, err := actionFind.Handle(h.storage, idInt)
	if err != nil {
		w.WriteHeader(422)
		return nil
	}
	data := syncWorkerResponse{
		ID:         item.ID,
		Status:     item.Status,
		StartedAt:  item.StartedAt.Format(time.RFC3339),
		FinishedAt: item.FinishedAt.Format(time.RFC3339),
	}
	resp := responseItem{
		Success: true,
		Data:    data,
	}
	js, err := json.Marshal(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)

	//w.Header().Set("Content-Type", "application/json")
	//w.Write(resp)
	//fmt.Fprintf(w, string(resp))
	//json.NewEncoder(w).Encode(resp)
	//
	//err := r.ParseForm()
	//if err != nil {
	//	return err
	//}
	//fmt.Println(r.Form.Get("id"))

	return nil
	//
	//w.Header().Set("Content-Type", "application/json")
	//
	//// aws sync start
	//h.clientRabbitMq.Publish([]byte("aws sync start"))

	//body, err := json.Marshal(resp)
	//if err != nil {
	//	log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	//}
	//w.WriteHeader(200)
	//fmt.Fprintf(w, string(body))
	//return nil
}
func (h *handler) Sync(w http.ResponseWriter, r *http.Request) error {
	w.WriteHeader(201)
	w.Write([]byte("post"))
	//UploadAll()
	//return fmt.Errorf("this is API error")
	return nil
}
