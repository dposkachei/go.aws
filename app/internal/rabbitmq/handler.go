package rabbitmq

import (
	"github.com/dposkachei/go-aws/app/internal/apperror"
	"github.com/dposkachei/go-aws/app/internal/config"
	"github.com/dposkachei/go-aws/app/internal/handlers"
	"github.com/dposkachei/go-aws/app/pkg/logging"
	"github.com/dposkachei/go-aws/app/pkg/queue/rabbitmq"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

var _ handlers.Handler = &handler{}

const (
	rabbitmqURL = "/rabbitmq"
)

type handler struct {
	logger *logging.Logger
}

type response struct {
	Success bool                   `json:"success"`
	Data    map[string]interface{} `json:"data"`
}

func NewHandler(logger *logging.Logger) handlers.Handler {
	return &handler{
		logger: logger,
	}
}

func (h *handler) Register(router *httprouter.Router) {
	router.HandlerFunc(http.MethodGet, rabbitmqURL, apperror.Middleware(h.Index))
}

func (h *handler) Index(w http.ResponseWriter, r *http.Request) error {
	w.Header().Set("Content-Type", "application/json")

	logger := logging.GetLogger()

	logger.Info("config initializing")
	cfg := config.GetConfig()

	cfgRabbitMQ := rabbitmq.ClientConfig{
		Host:     cfg.RabbitMQ.Host,
		Port:     cfg.RabbitMQ.Port,
		Username: cfg.RabbitMQ.Username,
		Password: cfg.RabbitMQ.Password,
		Queue:    cfg.RabbitMQ.Queue,
	}
	logger.Info("rabbitmq listening...")
	clientRabbitMq := rabbitmq.NewClient(&cfgRabbitMQ)

	logger.Info("rabbitmq publish...")
	clientRabbitMq.Publish([]byte("text 1"))

	w.WriteHeader(200)
	return nil
}
