package sync_items

import (
	"fmt"
	"github.com/dposkachei/go-aws/app/pkg/client/mysql"
)

const (
	STATUS_NOT_ACTIVE = 0
	STATUS_PENDING    = 1
	STATUS_PROCESSED  = 2
	STATUS_FINISHED   = 3
	STATUS_CANCELLED  = 4
	STATUS_DELETED    = 5
	STATUS_ERROR      = 6
)

type SyncItem struct {
	ID     int64  `json:"id"`
	Bucket string `json:"bucket"`
	Type   string `json:"type"`
	Input  string `json:"input"`
	Output string `json:"output"`
	Status int    `json:"status"`
}

func Create(client *mysql.StorageClient, item SyncItem) (int64, error) {
	sql := fmt.Sprintf(`
		INSERT INTO sync_items (bucket, type, input, output, status, created_at) VALUES ("%s", "%s", "%s", "%s", %d, current_timestamp());
	`, item.Bucket, item.Type, item.Input, item.Output, STATUS_PROCESSED)
	res, err := client.DB.Exec(sql)
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return id, err
}

func CreateSynced(client *mysql.StorageClient, item SyncItem) (int64, error) {
	sql := fmt.Sprintf(`
		INSERT INTO sync_items (bucket, type, input, output, status, created_at) VALUES ("%s", "%s", "%s", "%s", %d, current_timestamp());
	`, item.Bucket, item.Type, item.Input, item.Output, STATUS_PROCESSED)
	res, err := client.DB.Exec(sql)
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	err = UpdateStatusSynced(client, id)
	if err != nil {
		return 0, err
	}
	return id, err
}

func UpdateStatusProcessed(client *mysql.StorageClient, id int64) error {
	sql := fmt.Sprintf(`
		UPDATE sync_items SET status = %d, updated_at = current_timestamp() WHERE id = %d;
	`, STATUS_PROCESSED, id)
	_, err := client.DB.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

func ExistSynced(client *mysql.StorageClient, item SyncItem) (bool, error) {
	var err error
	sql := fmt.Sprintf(`
		SELECT COUNT(*) FROM sync_items WHERE type = "%s" AND bucket = "%s" AND input = "%s" AND output = "%s" AND status = %d LIMIT 1;
	`, item.Type, item.Bucket, item.Input, item.Output, STATUS_FINISHED)
	count := 0
	err = client.DB.QueryRow(sql).Scan(&count)
	if err != nil {
		return false, err
	}
	return count != 0, nil
	//for row.Next() {
	//	count += 1
	//}
	//if count == 0 {
	//	return 0, nil
	//}
	//fmt.Println(count)
	//fmt.Println(row)
	////if row.Err().Error() == sql.ErrNo {
	////
	////}
	//if err = row.Scan(&exist.ID, &exist.Input, &exist.Output, &exist.Status); err != nil {
	//	return 0, err
	//}
	//return exist.ID, nil
}

func UpdateStatusSynced(client *mysql.StorageClient, id int64) error {
	sql := fmt.Sprintf(`
		UPDATE sync_items SET status = %d, synced_at = current_timestamp(), updated_at = current_timestamp() WHERE id = %d;
	`, STATUS_FINISHED, id)
	_, err := client.DB.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

func UpdateStatusFailed(client *mysql.StorageClient, id int64, error string) error {
	sql := fmt.Sprintf(`
		UPDATE sync_items SET status = %d, error = %s, updated_at = current_timestamp() WHERE id = %d;
	`, STATUS_ERROR, error, id)
	_, err := client.DB.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}
