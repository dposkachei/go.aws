package sync_workers

import (
	"context"
	"fmt"
	"github.com/dposkachei/go-aws/app/internal/domain/sync_workers"
	"github.com/dposkachei/go-aws/app/pkg/logging"
)

type db struct {
	logger *logging.Logger
}

func (d *db) Create(ctx context.Context, item sync_workers.SyncWorker) (string, error) {
	d.logger.Debug("create user")
	return "", fmt.Errorf("failed to convert user, hex. probably oid: %s", item)
}
