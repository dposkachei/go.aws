package create

import (
	"fmt"
	"github.com/dposkachei/go-aws/app/internal/domain/sync_workers"
	"github.com/dposkachei/go-aws/app/pkg/client/mysql"
)

func Handle(client *mysql.StorageClient) (int64, error) {
	sql := fmt.Sprintf(`
		INSERT INTO sync_workers (status, created_at) VALUES (%d, current_timestamp());
	`, sync_workers.STATUS_NOT_ACTIVE)
	res, err := client.DB.Exec(sql)
	if err != nil {
		return 0, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return 0, err
	}
	return id, err
}
