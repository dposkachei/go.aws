package find

import (
	"fmt"
	"github.com/dposkachei/go-aws/app/internal/domain/sync_workers"
	"github.com/dposkachei/go-aws/app/pkg/client/mysql"
)

func Handle(client *mysql.StorageClient, id int) (*sync_workers.SyncWorker, error) {
	var item sync_workers.SyncWorker
	query := fmt.Sprintf(`
		SELECT id, status, started_at, finished_at FROM sync_workers WHERE id = %d LIMIT 1 ;
	`, id)
	err := client.DB.QueryRow(query).Scan(&item.ID, &item.Status, &item.StartedAt, &item.FinishedAt)
	if err != nil {
		return &item, err
	}
	return &item, err
}
