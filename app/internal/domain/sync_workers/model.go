package sync_workers

import (
	"fmt"
	"github.com/dposkachei/go-aws/app/pkg/client/mysql"
	"log"
	"time"
)

const (
	ID          = "id"
	STATUS      = "status"
	STARTED_AT  = "started_at"
	FINISHED_AT = "finished_at"
)

const (
	STATUS_NOT_ACTIVE = 0
	STATUS_ACTIVE     = 1
	STATUS_FINISHED   = 2
	STATUS_FAILED     = 3
)

type SyncWorker struct {
	ID         int       `json:"id"`
	Status     int       `json:"status"`
	StartedAt  time.Time `json:"started_at"`
	FinishedAt time.Time `json:"finished_at"`
}

func GetAll(client *mysql.StorageClient) (*[]SyncWorker, error) {
	query := fmt.Sprintf(`
		SELECT id, status FROM sync_workers;
	`)
	var syncWorkers []SyncWorker
	res, err := client.DB.Query(query)
	if err != nil {
		return &syncWorkers, err
	}
	for res.Next() {
		var syncWorker SyncWorker
		err = res.Scan(&syncWorker.ID, &syncWorker.Status)
		if err != nil {
			log.Fatal(err)
		}
		syncWorkers = append(syncWorkers, syncWorker)
	}
	fmt.Println(fmt.Sprintf("%v", syncWorkers))
	return &syncWorkers, err
}

func UpdateStatusStarted(client *mysql.StorageClient, id string) error {
	sql := fmt.Sprintf(`
		UPDATE sync_workers SET status = %d, started_at = current_timestamp(), updated_at = current_timestamp() WHERE id = %s;
	`, STATUS_ACTIVE, id)
	_, err := client.DB.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

func UpdateStatusFinished(client *mysql.StorageClient, id string) error {
	sql := fmt.Sprintf(`
		UPDATE sync_workers SET status = %d, finished_at = current_timestamp(), updated_at = current_timestamp() WHERE id = %s;
	`, STATUS_FINISHED, id)
	_, err := client.DB.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}

func UpdateStatusFailed(client *mysql.StorageClient, id string) error {
	sql := fmt.Sprintf(`
		UPDATE sync_workers SET status = %d, updated_at = current_timestamp() WHERE id = %s;
	`, STATUS_FAILED, id)
	_, err := client.DB.Exec(sql)
	if err != nil {
		return err
	}
	return nil
}
