package config

import (
	"fmt"
	"github.com/dposkachei/go-aws/app/pkg/utils"
	"github.com/ilyakaznacheev/cleanenv"
	"sync"
)

type Config struct {
	App struct {
		Debug bool   `env:"APP_DEBUG" env-required:"true" env-default:"false"`
		Env   string `env:"APP_ENV" env-required:"true" env-default:"default"`
	}
	Listen struct {
		Type   string `env:"LISTEN_TYPE" env-default:"port"`
		BindIP string `env:"LISTEN_IP"`
		Port   string `env:"LISTEN_PORT"`
	}
	Redis struct {
		Host     string `env:"REDIS_HOST" env-default:"localhost"`
		Port     string `env:"REDIS_PORT" env-default:"6379"`
		User     string `env:"REDIS_USER" env-default:""`
		Password string `env:"REDIS_PASSWORD" env-default:""`
		Database int    `env:"REDIS_DATABASE" env-default:"0"`
	}
	RabbitMQ struct {
		Host     string `env:"RABBITMQ_HOST" env-default:"localhost"`
		Port     string `env:"RABBITMQ_PORT" env-default:"5672"`
		Username string `env:"RABBITMQ_USERNAME" env-default:""`
		Password string `env:"RABBITMQ_PASSWORD" env-default:""`
		Queue    string `env:"RABBITMQ_QUEUE" env-default:""`
	}
	MySQL struct {
		Host     string `env:"MYSQL_HOST" env-default:"localhost"`
		Port     string `env:"MYSQL_PORT" env-default:"3306"`
		Username string `env:"MYSQL_USERNAME" env-default:""`
		Password string `env:"MYSQL_PASSWORD" env-default:""`
		Database string `env:"MYSQL_DATABASE" env-default:""`
	}
	Aws struct {
		Enabled   bool   `env:"AWS_ENABLED" env-default:"false"`
		KeyID     string `env:"AWS_KEY_ID" env-default:"default"`
		SecretKey string `env:"AWS_SECRET_KEY" env-default:"default"`
		Bucket    string `env:"AWS_BUCKET" env-default:"default"`
		Region    string `env:"AWS_REGION" env-default:"default"`
		Host      string `env:"AWS_HOST" env-default:"default"`
		PathFrom  string `env:"AWS_PATH_FROM" env-default:""`
		PathTo    string `env:"AWS_PATH_TO" env-default:""`
	}
	Slack struct {
		Enabled  bool   `env:"SLACK_ENABLED" env-default:"false"`
		Webhook  string `env:"SLACK_WEBHOOK" env-default:"default"`
		Username string `env:"SLACK_USERNAME" env-default:"default"`
		Channel  string `env:"SLACK_CHANNEL" env-default:"default"`
	}
	Logger struct {
		Enabled bool   `env:"LOG_ENABLED" env-default:"false"`
		Path    string `env:"LOG_PATH" env-default:"default"`
	}
}

var instance *Config
var once sync.Once

func GetConfig() *Config {
	once.Do(func() {
		//cfg := logging.ClientConfig{
		//	Path:    "",
		//	Enabled: true,
		//}
		//logger := logging.GetLogger(&cfg)
		//logger.Info("read application config")

		instance = &Config{}
		if err := cleanenv.ReadConfig(utils.BaseDirFile(".env"), instance); err != nil {
			helpText := "Basic system"
			help, _ := cleanenv.GetDescription(instance, &helpText)
			fmt.Println(help)
			fmt.Errorf("%s", err)
			//fmt.Println(instance.App.Env)
			//logger.Info(help)
			//logger.Fatal(err)
		}
	})
	return instance
}
