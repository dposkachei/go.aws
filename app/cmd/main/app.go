package main

import (
	"context"
	"encoding/json"
	"fmt"
	internalAws "github.com/dposkachei/go-aws/app/internal/aws"
	"github.com/dposkachei/go-aws/app/internal/config"
	"github.com/dposkachei/go-aws/app/internal/domain/sync_workers"
	internalRabbitmq "github.com/dposkachei/go-aws/app/internal/rabbitmq"
	"github.com/dposkachei/go-aws/app/pkg/client/mysql"
	"github.com/dposkachei/go-aws/app/pkg/logging"
	"github.com/dposkachei/go-aws/app/pkg/queue/rabbitmq"
	pkgServer "github.com/dposkachei/go-aws/app/pkg/server"
	"github.com/julienschmidt/httprouter"
)

const (
	InfoConfigInitial     = "config initializing"
	InfoRouterInitial     = "router initializing"
	InfoRabbitMQInitial   = "rabbitmq initializing"
	InfoRabbitMQListening = "rabbitmq listening"
	InfoRedisInitial      = "redis initializing"
	InfoServerInitial     = "server initializing"
	InfoServerStarting    = "server starting"
	InfoMySQLInitial      = "mysql initializing"
)

func main() {
	//var err error
	logger := logging.GetLogger()

	logger.Default("------------------------------------------------------")

	logger.InfoSi(InfoConfigInitial)
	cfg := config.GetConfig()
	logger.Success(InfoConfigInitial)
	logger.Success("ENV: " + cfg.App.Env)

	logger.InfoSi(InfoRouterInitial)
	router := httprouter.New()
	logger.Success(InfoRouterInitial)

	logger.InfoSi(InfoRabbitMQListening)
	clientRabbitMq := rabbitmq.NewClient(&rabbitmq.ClientConfig{
		Host:     cfg.RabbitMQ.Host,
		Port:     cfg.RabbitMQ.Port,
		Username: cfg.RabbitMQ.Username,
		Password: cfg.RabbitMQ.Password,
		Queue:    cfg.RabbitMQ.Queue,
	})
	logger.Success(InfoRabbitMQListening)

	logger.InfoSi(InfoMySQLInitial)
	clientMySQL, err := mysql.NewClient(context.TODO(), &mysql.StorageConfig{
		Host:     cfg.MySQL.Host,
		Port:     cfg.MySQL.Port,
		Database: cfg.MySQL.Database,
		Username: cfg.MySQL.Username,
		Password: cfg.MySQL.Password,
	})
	if err != nil {
		logger.Error(InfoMySQLInitial)
		panic(err)
	}
	logger.Success(InfoMySQLInitial)
	//aws.UploadAll()

	handlerAws := internalAws.NewHandler(logger, clientRabbitMq, clientMySQL)
	handlerAws.Register(router)

	handlerRabbitMq := internalRabbitmq.NewHandler(logger)
	handlerRabbitMq.Register(router)

	//logger.InfoSi(InfoRedisInitial)
	//clientRedis := redis.NewClient(&redis.ClientConfig{
	//	Host:     cfg.Redis.Host,
	//	Port:     cfg.Redis.Port,
	//	User:     cfg.Redis.User,
	//	Password: cfg.Redis.Password,
	//	Database: cfg.Redis.Database,
	//})
	//logger.Success(InfoRedisInitial)

	// REDIS
	//logger.Info("redis set: value11")
	//err = clientRedis.Set(context.TODO(), "test", "value11")
	//if err != nil {
	//	panic(err)
	//}
	//
	//logger.Info("redis get")
	//val, err := clientRedis.Get(context.TODO(), "test")
	//if err != nil {
	//	panic(err)
	//}
	//logger.Info(fmt.Sprintf("redis get is: %s", val))

	logger.InfoSi(InfoServerStarting)
	pkgServer.StartServer(router, cfg)
	logger.Success(InfoServerStarting)
	logger.Success(fmt.Sprintf(":%s", cfg.Listen.Port))

	//
	//logger.Info("rabbitmq publish...")
	//clientRabbitMq.Publish([]byte("text 1"))

	logger.Info("rabbitmq consume...")
	clientRabbitMq.Consume(func(val interface{}) error {
		v := fmt.Sprintf("%s", val)
		//fmt.Println(val)
		//fmt.Println(v)
		byt := []byte(v)
		var me rabbitmq.Message
		if err = json.Unmarshal(byt, &me); err != nil {
			panic(err)
		}
		fmt.Println(me)
		fmt.Println(me.Type)
		fmt.Println(me.ID)
		if me.Type == rabbitmq.TYPE_AWS_SYNC {
			// start sync
			go func() {
				if err = sync_workers.UpdateStatusStarted(clientMySQL, me.ID); err != nil {
					panic(err)
				}
				_, errUp := internalAws.UploadAll(clientMySQL)
				if errUp != nil {
					if err = sync_workers.UpdateStatusFailed(clientMySQL, me.ID); err != nil {
						panic(err)
					}
					panic(errUp)
				}
				if err = sync_workers.UpdateStatusFinished(clientMySQL, me.ID); err != nil {
					panic(err)
				}
			}()
		}
		return nil
	})
	//
	//logger.Info("rabbitmq publish...")
	//clientRabbitMq.Publish([]byte("text 2"))
}
