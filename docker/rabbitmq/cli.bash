
queue-delete user

exchange-delete email

queue-declare user

exchange-declare email

queue-bind user email


rabbitmqctl list_queues
rabbitmqctl

rabbitmqctl add_user "golang" "golang"

rabbitmqctl add_user golang golang
rabbitmqctl set_permissions -p / golang ".*" ".*" ".*"
rabbitmqctl set_user_tags golang administrator

rabbitmqctl list_bindings

Источник: https://27sysday.ru/programmirovanie/laravel/laravel-rabbitmq-s-nulya-i-dlya-novichkov

Теперь необходимо настроить RabbitMQ, переходим во вкладку exchanges и создаем новый.
https://27sysday.ru/wp-content/uploads/2021/12/2021-12-18_08-05-55-1024x506.png

Затем переходим в Queues и создаем новую очередь.
https://27sysday.ru/wp-content/uploads/2021/12/2021-12-18_08-07-50-1024x497.png

Дальше у вас появится новая очередь confirm нажимаем на неё, и связываем exchanges с Queues. Заходим в очередь confirm, просто нажмите на неё и выберите пункт Bindings.
https://27sysday.ru/wp-content/uploads/2021/12/2021-12-18_08-11-45.png

Публикация сообщений
https://www.tutlane.com/tutorial/rabbitmq/rabbitmq-publish-messages-to-queue