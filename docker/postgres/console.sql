-----------------------------------------------------------
-- authors
-----------------------------------------------------------
-- up
CREATE TABLE authors
(
    id   UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name VARCHAR(100) NOT NULL
);

-- down
DROP TABLE IF EXISTS authors;

-----------------------------------------------------------
-- books
-----------------------------------------------------------
-- up
CREATE TABLE books
(
    id        UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    name      VARCHAR(100) NOT NULL,
    author_id UUID         NOT NULL,
    CONSTRAINT authors_fk FOREIGN KEY (author_id) REFERENCES authors (id) ON DELETE CASCADE
);

-- down
ALTER TABLE books
DROP
CONSTRAINT authors_fk;
--
DROP TABLE IF EXISTS books;


-----------------------------------------------------------
-- book_author
-----------------------------------------------------------
-- up
CREATE TABLE book_author
(
    book_id   UUID NOT NULL,
    author_id UUID NOT NULL,
    PRIMARY KEY (book_id, author_id),
    CONSTRAINT book_fk FOREIGN KEY (book_id) REFERENCES books (id) ON DELETE CASCADE,
    CONSTRAINT author_fk FOREIGN KEY (author_id) REFERENCES authors (id) ON DELETE CASCADE
)

-- down
ALTER TABLE book_author
DROP
CONSTRAINT book_fk;
--
ALTER TABLE book_author
DROP
CONSTRAINT author_fk;
--
DROP TABLE IF EXISTS book_author;



-----------------------------------------------------------
-- insert
-----------------------------------------------------------
INSERT INTO authors (name)
VALUES ('Народ'); -- eb05863d-4252-4eea-83ec-a1010832ac94
INSERT INTO authors (name)
VALUES ('Автор 1'); -- 5425b6f2-4fa2-4b7c-bf1a-6623d719650d
INSERT INTO authors (name)
VALUES ('Автор 2'); -- 26b3e75f-6982-4384-8014-af27f82fe770

INSERT INTO books (name, author_id)
VALUES ('Книга 1', 'eb05863d-4252-4eea-83ec-a1010832ac94');
INSERT INTO books (name, author_id)
VALUES ('Книга 2', '5425b6f2-4fa2-4b7c-bf1a-6623d719650d');
INSERT INTO books (name, author_id)
VALUES ('Книга 3', '26b3e75f-6982-4384-8014-af27f82fe770');