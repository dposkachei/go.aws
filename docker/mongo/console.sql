use test
use default


db.createCollection("default")

db.getCollection("default").drop();

db.default.insertOne({
    "test": 1,
})

db.default.get("test")

// show all databases
show dbs

// show all collections
show collections

// count
db.default.countDocuments()

// find one
db.default.findOne()
db.default.findOne().limit(10)
db.default.find({"name.family": "Smith"}).count()
db.default.find({likes: {$gt: 1}})
db.default.find({likes: 0})
db.default.find({likes: {$ne: 1}})
db.default.find().sort({age: 1})
db.default.find().sort({age: -1})

// indexes
db.default.createIndex({"name.family": 1})
db.default.createIndex({email: 1}, {unique: true})
db.default.getIndexes()
db.default.dropIndex("name.given_1")

db.default.findOne({_id: ObjectId("625876caf05c3e38cddb8af2")})