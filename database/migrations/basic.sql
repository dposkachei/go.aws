-- ---------------------------------------------------------
-- sync_workers
-- ---------------------------------------------------------
-- up
CREATE TABLE sync_workers
(
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `status` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
    `started_at` TIMESTAMP NULL DEFAULT NULL,
    `finished_at` TIMESTAMP NULL DEFAULT NULL,
    `created_at` TIMESTAMP NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
)
    COLLATE='utf8mb4_unicode_ci'
    ENGINE=InnoDB
;

-- CREATE TABLE sync_workers
-- (
--     id VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
--     `status` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
--     `started_at` TIMESTAMP NULL DEFAULT NULL,
--     `finished_at` TIMESTAMP NULL DEFAULT NULL,
--     `created_at` TIMESTAMP NULL DEFAULT NULL,
--     `updated_at` TIMESTAMP NULL DEFAULT NULL,
--     PRIMARY KEY (`id`) USING BTREE
-- )
--     COLLATE='utf8mb4_unicode_ci'
--     ENGINE=InnoDB
-- ;

-- down
DROP TABLE IF EXISTS sync_workers;

-- insert
INSERT INTO sync_workers (id, status, started_at, finished_at, created_at, updated_at)
VALUES (uuid(), 1, current_timestamp(), current_timestamp(), current_timestamp(), current_timestamp());

-- insert
INSERT INTO sync_workers (status, started_at, finished_at, created_at, updated_at)
VALUES (1, current_timestamp(), current_timestamp(), current_timestamp(), current_timestamp());


-- ---------------------------------------------------------
-- sync_items
-- ---------------------------------------------------------
-- up
CREATE TABLE `sync_items` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `bucket` VARCHAR(255) NOT NULL,
    `type` VARCHAR(255) NOT NULL,
    `input` VARCHAR(255) NULL DEFAULT NULL,
    `output` VARCHAR(255) NULL DEFAULT NULL ,
    `error` TEXT NULL DEFAULT NULL,
    `synced_at` TIMESTAMP NULL DEFAULT NULL,
    `status` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
    `created_at` TIMESTAMP NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
)
    COLLATE='utf8mb4_unicode_ci'
    ENGINE=InnoDB
;

-- down
DROP TABLE IF EXISTS sync_items;

-- insert
INSERT INTO sync_items (id, bucket, type, input, output, synced_at, status, created_at, updated_at)
VALUES (1, "dp-golang", "upload", "/var/www/go.aws/storage/file", "storage/file", current_timestamp(), "1", current_timestamp(), current_timestamp());