#!/usr/bin/env make

.PHONY : help build run vendor

help: ## Показать эту подсказку
	@echo "Сборка"

vendor: ## Установить зависимости
	cd app && go mod vendor
	cd app && go mod tidy

build: ## Билд версии
	cd app && go build -o build/aws github.com/dposkachei/go-aws/app/cmd/main

run: ## Запустить
	./app/build/aws